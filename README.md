#Docker Repository For Tofu by misosil v2

### Base Docker Image
* [ubuntu:14.04](https://registry.hub.docker.com/_/ubuntu/)

## Installation

1. [Docker](https://www.docker.com/)をインストールする

2. Download [automated build](https://registry.hub.docker.com/u/tadao/tofu/) from private [Docker Hub Registry](https://registry.hub.docker.com/):

```bash
docker pull tadao/docker-tofu
```

### Usage

```bash
docker run -d -p 80:80 tadao/docker-tofu
```

After few seconds, open `http://<host>` to see the Flask app.
